<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Auth::routes();
route::get('/notify','NotifyController@show')->middleware('auth');
route::post('/notify/{id}','NotifyController@edit')->middleware('auth');
Route::get('/yard','YardController@show')->middleware('auth');//->middleware('auth');
Route::post('/add','YardController@add')->middleware('auth');
Route::post('/edit/{id}','YardController@edit')->middleware('auth');
Route::post('/delete/{id}','YardController@delete')->middleware('auth');
Route::post('/login',function(){
	$data['username'] = Request::get('username');
	$data['password'] = Request::get('password');
	// echo bcrypt('123');
	if(Auth::attempt($data)){
		return redirect(url('home'));
	}
	return redirect(url('login')); 
});
Route::get('/logout',function(){
	Auth::logout();
	return redirect(url('login'));
});

// interview
Route::group(['prefix'=>'/interview','middleware'=>'auth'],function(){
	Route::get('/','InterviewController@index');
	Route::post('/do_add','InterviewController@do_add');
	// Route::get('/edit/{id}','ControllerNews@edit');
	Route::post('/do_edit/{id}','InterviewController@do_edit');
	// Route::get('/delete/{id}','ControllerNews@delete');
});

// begin room test
Route::get('/roomtest', 'RoomTestController@index');
//end room test

Route::get('/getnoti', 'GetNoti@index');
Route::get('/gettable', 'GetTable@index');
Route::post('/adduser', 'AddUserToTest@index');
Route::get('/home', 'HomeController@index')->middleware('auth');

Route::get('/', 'Form_register@form_register');
Route::post('/do_add', 'Form_register@do_add');
route::get('/detail','DetailController@list')->middleware('auth');
route::post('/edit_cv_online/{id}','DetailController@edit_cv_online')->middleware('auth');

