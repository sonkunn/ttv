<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
class YardController extends Controller
{
    // public function __construct(){
    //     $this->middleware('auth');
    // }
	public function show(){
		$arr_student=DB::table('student')->where('is_come','=',1)->get();

		return view('yard',array(
			'arr_student'=>$arr_student,
			));
	}
    public function add(Request $request){
    	$data=$request->all();
    	$id_student=$data['id_student'];
    	$id_cv_offline=$data['id_cv_offline'];

    	DB::table('student')->where('id_student','=',$id_student)->update([
    			'is_come'=>1,
    			'id_cv_offline'=>$id_cv_offline,
    		]);
    	return redirect('yard');
    }
    public function edit(Request $request,$id){
        $data=$request->all();
        $id_cv_offline=$data['id_cv_offline'];
        DB::table('student')->where('id','=',$id)->update([
                'id_cv_offline'=>$id_cv_offline,
            ]);
        return redirect('yard');
    }
    public function delete($id){
        DB::table('student')->where('id','=',$id)->update(
                ['is_come'=>2,]
            );
        return redirect('yard');
    }
}
