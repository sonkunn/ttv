<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class GetTable extends Controller
{
    public function index()
    {
	    //config db access
	    $server = 'localhost:3306';
	    $user = 'root';
	    $password = '';
	    $database = 'ttv';
	    $conn = mysqli_connect($server, $user, $password, $database);
	    mysqli_set_charset($conn, "UTF8");

		//query data
	    $sql = "SELECT * FROM test, users where test.user_id = users.id ORDER BY test.time_come DESC";
	    $result = mysqli_query($conn, $sql);
	    $arr = array();
	    while($rows = mysqli_fetch_assoc($result)) {
		    $arr[] = $rows;
	    }

		//response
	    if(isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'GET') {
		    if(isset($_GET['action']) && $_GET['action'] == 'changeStt') {
			    $id = $_GET['id'];
			    $query = "UPDATE test SET is_done = 1 where test.user_id = $id";
			    mysqli_query($conn, $query);
		    }else {
			    echo json_encode($arr);
		    }
	    }
    }
}
