<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Auth;
class InterviewController extends Controller
{
    public function index(){
        $id_users = Auth::user()->id;
    	$arr_interview = DB::table('interview')->where('id_users','=',$id_users)->get();
    	foreach ($arr_interview as $value) {
    		$students = DB::table('student')->where('id','=',$value->id_student)->first();
    		$sum_point = 0;
    		$sum_point += $value->point_cv;
    		$sum_point += $value->point_free_time;
    		$sum_point += $value->point_creative;
    		$sum_point += $value->point_dynamic;
    		$sum_point += $value->point_clothes;
    		$sum_point += $value->point_on_time;
    		$sum_point += $value->point_body_language;
    		$sum_point += $value->point_situation;
    		$sum_point += $value->point_communication;
    		$sum_point += $value->point_talent;

    		$value->name = $students->name;
    		$value->msv = $students->id_student;
    		$value->sum_point = $sum_point;
    	}
    	return view('interview', array(
			'arr_interview'=>$arr_interview,
		));
    }
    public function do_add(Request $request) {
    	$data = $request->all();
    	$id_users = Auth::user()->id;
    	$id_student=$data['id_student'];
    	$students = DB::table('student')->where('id_student','=',$id_student)->first();
    	$value = 0;
    	if(isset($students) && isset($id_users)) {
			$interviews = DB::table('interview')->where('id_student','=',$students->id)
	    									   // ->where('id_users','=',$id_users)
	    									   ->first();
	 		if(isset($interviews)) {
	 			return redirect(url('interview'));
	 		}

	        DB::table('interview')->insert([
	                'id_users'=>$id_users,
	                'id_student' => $students->id,
	                'point_cv' => $value,
	                'point_free_time' =>$value,
	                'point_creative' => $value,
	                'point_dynamic'=>$value,
	                'point_clothes'=>$value,
	                'point_on_time'=>$value,
	                'point_body_language'=>$value,
	                'point_situation'=>$value,
	                'point_communication'=>$value,
	                'point_talent'=>$value,
	                'note'=> 'Nothing',
	            ]);
	    }
    	return redirect(url('interview'));
    }
     public function do_edit($id,Request $request) {
    	$data = $request->all();
    	$id_users = Auth::user()->id;

    	$point_cv=$data['point_cv'];
    	$point_free_time=$data['point_free_time'];
    	$point_creative=$data['point_creative'];
    	$point_dynamic=$data['point_dynamic'];
    	$point_clothes=$data['point_clothes'];
    	$point_on_time=$data['point_on_time'];
    	$point_body_language=$data['point_body_language'];
    	$point_situation=$data['point_situation'];
    	$point_communication=$data['point_communication'];
    	$point_talent=$data['point_talent'];
    	$note=$data['note'];

    	$interviews = DB::table('interview')->where('id_student','=',$id)
    									   ->where('id_users','=',$id_users)
    									   ->update([
                'point_cv' => $point_cv,
                'point_free_time' =>$point_free_time,
                'point_creative' => $point_creative,
                'point_dynamic'=>$point_dynamic,
                'point_clothes'=>$point_clothes,
                'point_on_time'=>$point_on_time,
                'point_body_language'=>$point_body_language,
                'point_situation'=>$point_situation,
                'point_communication'=>$point_communication,
                'point_talent'=>$point_talent,
                'note'=> $note,
			]);
    	return redirect(url('interview'));
    }
}
