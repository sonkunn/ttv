<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class GetNoti extends Controller
{
    public function index()
    {
	    $server = 'localhost:3306';
	    $user = 'root';
	    $password = '';
	    $database = 'ttv';
	    $conn = mysqli_connect($server, $user, $password, $database);
	    mysqli_set_charset($conn, "UTF8");
	    $room = 1;
	    date_default_timezone_set('Asia/Ho_Chi_Minh');
	    $max_time_test = 30 * 60;
	    $arr_test = mysqli_query($conn, "SELECT test.*, users.name, users.id FROM test, users where users.id = test.user_id and test.room = $room and test.is_done = 0 ORDER BY test.time_come ASC");
	    $arr = array();
	    while($rows = mysqli_fetch_assoc($arr_test)) {
		    $time_come = $rows['time_come'];
		    $time_come = strtotime(date($time_come)) + $max_time_test;
		    $current_time = time();
		    $time_avai = round(($time_come - $current_time)/60);
		    if($time_avai <= 10) {
			    if($time_avai <= 0) {
				    $time_avai = "Time out!!";
			    }else {
				    $time_avai = $time_avai . " mins";
			    }
			    $rows = array_merge($rows, array('time_avai' => $time_avai,));
			    $arr[] = $rows;
		    }
	    }
		//echo '<pre>';
		//print_r($arr);
	    echo json_encode($arr);

    }
}
