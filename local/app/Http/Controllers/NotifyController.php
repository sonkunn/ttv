<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class NotifyController extends Controller
{
    public function  show(){
    	$arr_student = DB::table('student')->orderby('created_at',"asc")->get();

		return view('notify',array(
			'arr_student' =>$arr_student,
			));

    }
    public function edit($id, Request $request){
    	$data = $request->all();
    	$time_interview = $data['time_interview'];
    	$note_cv_online =$data['note_cv_online'];
		$message = isset($data['message']);
		$relymessage = isset($data['relymessage']);
		$email = isset($data['email']);
		$relyemail = isset($data['relyemail']);
		DB::table('student')
		                    ->where('id','=',$id)
		                    ->update([
		                    	'time_interview'=>$time_interview,
		                    	'note_cv_online'=>$note_cv_online,
		                    	'is_message'=>$message,
		                    	'is_email'=>$email,
		                    	'reply_message'=>$relymessage,
		                    	'reply_email'=>$relyemail,
							]);
        return redirect(url('notify'));
        
    }
}
