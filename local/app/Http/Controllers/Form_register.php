<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
class Form_register extends Controller
{
    public function form_register()
    {
    	return view('Form_register',array());
    }
    public function do_add(Request $request)
    {
    	$data = $request->all();
    	$name = $data['name'];
    	$id_student = $data['id_student'];
    	$class = $data['class'];
    	$email = $data['email'];
    	$phone_number = $data['phone_number'];
    	$home_town = $data['home_town'];
    	$address = $data['address'];
    	$media = $data['media'];
    	$free_time = $data['free_time'];
    	$hope = $data['hope'];
    	$another_club = $data['another_club'];
    	$reason_choose = $data['reason_choose'];

        $check = DB::table('student')->where('id_student','=',$id_student)->first();

        if(isset($check)){
            return redirect(url('/'.'?error=true'));
        }else{
            DB::table('student')->insert([
                'name'=>$name,
                'id_student' => $id_student,
                'class' => $class,
                'email' =>$email,
                'phone_number' =>$phone_number,
                'home_town' => $home_town,
                'address' =>$address,
                'media' => $media,
                'free_time' => $free_time,
                'hope' => $hope,
                'another_club' => $another_club,
                'reason_choose' => $reason_choose,
            ]);
            return view("success");
        }
    }
}
