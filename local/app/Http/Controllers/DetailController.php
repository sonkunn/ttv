<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
class DetailController extends Controller
{
    public function list(){
    	$arr_student = DB::table('student')->get();
    	return view('detail', array(
    		'arr_student' => $arr_student,
    	));
    }

    public function edit_cv_online($id, Request $request) {
    	$data = $request->all();
    	$note_cv_online = $data['note_cv_online'];
    	DB::table('student')->where('id','=',$id)->update([
			
			'note_cv_online'=>$note_cv_online,
			]);
		return redirect(url('detail'));
    }
}
