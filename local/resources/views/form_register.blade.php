@extends('layouts.layout')
@section('content')
        <style>
            .banner {
                position: relative;
                width: 100%;
                overflow: hidden;
                line-height: 0;
                height: 333px;
            }
            .space {
                margin: 20px;
            }
            .must {
                color: red;
            }
        </style>
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
        <!-- END HEAD -->

        <!-- BEGIN HEADER -->
        <div class="page-container">
        
        <div class="banner">
            <img src="{{asset('')}}" alt="">
        </div>
        
        <div class="page-content">
            <div class="row" >
                <div class="col-md-6 col-md-offset-3 portlet light" style="margin-top: -50px;">
                    <center><h1 class="page-title space"> Đơn đăng ký tham gia CLB Lập trình PTIT</h1></center>
                    @if(isset($_GET['error']) && $_GET['error']=="true")
                    	<center><h1 class="page-title space" style="color: red"><b>bạn bị trùng mã sinh viên</b></h1></center>
                    @endif
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">
                                <span class="caption-subject bold uppercase"> <h6>Các bạn hãy đăng ký theo biểu mẫu dưới này nhé!</h6></span>
                                <p><span><small><span class="must">(*)</span> Đây là những phần bắt buộc</small></span></p>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form role="form" method="post" action="{!! url('do_add') !!}" enctype="multipart/form-data" >
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-body">
                                    <div class="form-group form-md-line-input">
                                        <input id="name" type="text" class="form-control" placeholder="" name="name" required>
                                        <label for="form_control_1">Họ và tên <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <input id="id_student" type="text" class="form-control" placeholder="" name="id_student" required>
                                        <label for="form_control_1">Mã Sinh Viên <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <input id="class" type="text" class="form-control" placeholder="" name="class" required>
                                        <label for="form_control_1">Lớp <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <input id="email" type="email" class="form-control" placeholder="" name="email" required> 
                                        <label for="form_control_1">Email <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <input id="phone_number" type="text" class="form-control" placeholder="" name="phone_number" required>
                                        <label for="form_control_1">Số điện thoại <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <input id="home_town" type="text" class="form-control" placeholder="" name="home_town" required>
                                        <label for="form_control_1">Quê quán <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <textarea id="address" class="form-control" rows="3" placeholder="" name="address"></textarea required>
                                        <label for="form_control_1">Nơi ở hiện tại? Cách học viện bao xa? Phương tiện di chuyển? <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-radios">
                                        <label>Bạn biết đến CLB qua nguồn thông tin nào? <span class="must">*</span></label>
                                        <div class="md-radio-list">
                                            <div class="md-radio">
                                                <input type="radio" class="md-radiobtn" id="know_club1" name="media" value="Fanpage CLB Lập trình PTIT" required>
                                                <label for="know_club1" >
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Fanpage CLB Lập trình PTIT </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="know_club2" name="media" class="md-radiobtn" value="Tờ rơi" required>
                                                <label for="know_club2" >
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Tờ rơi </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="know_club3" name="media" class="md-radiobtn" value="Bạn bè" required>
                                                <label for="know_club3" >
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Bạn bè </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" id="know_club4" name="media" class="md-radiobtn" value="Truyền thông tại lớp học" required>
                                                <label for="know_club4" >
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> Truyền thông tại lớp học </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <textarea id="free_time" class="form-control" rows="3" placeholder="" name="free_time" required></textarea>
                                        <label for="form_control_1">Bạn có thể tham gia hoạt động cùng CLB vào những khoảng thời gian nào trong tuần? <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <textarea id="hope" class="form-control" rows="3" placeholder="" name="hope" required></textarea >
                                        <label for="form_control_1">Bạn có mong muốn gì khi tham gia CLB? <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <textarea id="another_club" class="form-control" rows="3" placeholder="" style="padding-top: 30px;" name="another_club" required></textarea>
                                        <label for="form_control_1">Bạn có đang tham gia CLB nào khác không? Nếu có thì đó là CLB nào? Bạn nghĩ mình đủ thời gian khi tham gia cả 2 CLB hay không <span class="must">*</span></label>
                                    </div>
                                    <div class="form-group form-md-line-input">
                                        <textarea id="reason_choose" class="form-control" rows="3" placeholder="" name="reason_choose" required></textarea>
                                        <label for="form_control_1">Vì sao chúng tôi nên chọn bạn?<span class="must">*</span></label>
                                    </div>
                                </div>
                                <div class="form-actions noborder">
                                    <input type="submit" class="btn blue" value="Gửi">
                                    <input type="reset" class="btn default" value="Hủy">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection