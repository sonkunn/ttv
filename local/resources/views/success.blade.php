@extends('layouts.layout')
@section('content') 
    <div class="page-container">
        <div class="page-content">
            <div class="container">
                <div class="row" >
                <div class="col-md-6 col-md-offset-3 portlet light">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Thông báo thành công</h3>
                        </div>
                        <div class="panel-body">
                            <p>Các bạn xem thêm thông tin tại <a href="https://www.facebook.com/clubproptit">Fanpage</a></p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection