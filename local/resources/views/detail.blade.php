@extends('layouts.layout')
@section('content')
	 <!-- MAIN  -->
    <div class="page-container" style="margin-top: 50px">
        <div class="page-content">
            <div class="row" >
                <div class="col-md-10 col-md-offset-1 portlet light">
                    <!-- END EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit portlet-datatable bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-dark"></i>
                                <span class="caption-subject font-red sbold uppercase">Table</span>
                            </div>
                            <div class="actions">
                                {{-- <button class="btn btn-sm dark" data-toggle="modal" href="#student">
                                    ADD NEW
                                </button> --}}
                                 <div class="modal fade in" id="student" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Detail</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="#" class="form-horizontal" id="form_sample_1" method="post">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label" for="form_control_1">MSV
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-10">
                                                                <input type="text" class="form-control" placeholder="" name="#">
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">...</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="submit" class="btn btn-dark">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                </form>
                                            </div>

                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table id="table_student_1" class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                                <thead>
                                <tr>
                                    <th> Manage </th>
                                    <th> Name </th>
                                    <th> Student Code </th>
                                    <th> Email </th>
                                    <th> Phone number </th>
                                    <th> Note CV online </th>
                                    <th> </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($arr_student as $row)
                                    <tr>
                                        <td>
                                            <button class="btn btn-sm dark" data-toggle="modal" href="#student{{$row->id_student}}">

                                                <i class="fa fa-search"></i>
                                            </button>
                                            <button class="btn btn-sm gray" data-toggle="modal" href="#notestudent{{$row->id_student}}">

                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <div class="modal fade bs-modal-lg" id="student{{$row->id_student}}" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><strong>Name:</strong> <span>{{$row->name}}</span></p>
                                                            <p><strong>Student Code:</strong> <span>{{$row->id_student}}</span></p>
                                                            <p><strong>Email:</strong> <span>{{$row->email}}</span></p>
                                                            <p><strong>Phone number:</strong> <span>{{$row->phone_number}}</span></p>
                                                            <p><strong>Hometown:</strong> <span>{{$row->home_town}}</span></p>
                                                            <p><strong>Address:</strong> <span>{{$row->address}}</span></p>
                                                            <p><strong>Media:</strong> <span>{{$row->media}}</span></p>
                                                            <p><strong>Free time:</strong> <span>{{$row->free_time}}</span></p>
                                                            <p><strong>Hope:</strong> <span>{{$row->hope}}</span></p>
                                                            <p><strong>Join Another Club:</strong> <span>{{$row->another_club}}</span></p>
                                                            <p><strong>Reason choose:</strong> <span>{{$row->reason_choose}}</span></p>
                                                            <p><strong>Time interview:</strong> <span>{{$row->time_interview}}</span></p>
                                                            <p><strong>Send Message:</strong> <span class="label label-success">1</span>||<strong>Rely Message:</strong> <span class="label label-success">1</span></p>
                                                            <p><strong>Send Mail:</strong> <span class="label label-success">1</span>||<strong>Rely Mail:</strong> <span class="label label-success">1</span></p>
                                                            <p><strong>NoteCV online:</strong> <span>{{$row->note_cv_online}}</span></p>
                                                            <p><strong>CVoffline:</strong> <span>{{$row->id_cv_offline}}</span></p>
                                                            <p><strong>Pass:</strong> <span>{{$row->is_pass}}</span></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>

                                            <div class="modal fade in" id="notestudent{{$row->id_student}}" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{url('edit_cv_online/'.$row->id)}}" class="form-horizontal" id="form_sample_1" method="post">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                
                                                                <div class="form-body">
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-2 control-label" for="form_control_1">Note
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-10">
                                                                            <input type="text" class="form-control" placeholder="" name="note_cv_online" value="{{$row->note_cv_online}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">note cv online</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="submit" class="btn btn-dark">
                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                            </form>
                                                        </div>

                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        
                                        
                                        <td> {{$row->name}}</td>
                                        <td> {{$row->id_student}}</td>
                                        <td> {{$row->email}} </td>
                                        <td> {{$row->phone_number}} </td>
                                        <td> {{$row->note_cv_online}} </td>
                                        <td> 
                                        
                                        </td>
                                        
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection