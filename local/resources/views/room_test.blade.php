@extends('layouts.layout')
@section('content')
    <div class="page-content">
        <div class="row" >
            <div class="col-md-8 portlet light">
                <!-- END EXAMPLE TABLE PORTLET-->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-dark"></i>
                            <span class="caption-subject font-red sbold uppercase">Table</span>
                        </div>
                        <div class="actions">
                            <button class="btn btn-sm dark" data-toggle="modal" href="#student">
                                ADD NEW
                            </button>
                            <div class="modal fade in" id="student" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                            <h4 class="modal-title">Detail</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{url('/adduser')}}" class="form-horizontal" id="form_sample_1" method="post">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="form-body">
                                                    <div class="form-group form-md-line-input">
                                                        <label class="col-md-2 control-label" for="form_control_1">MSV
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-10">
                                                            <input type="text" class="form-control" placeholder="" name="student_id">
                                                            <div class="form-control-focus"> </div>
                                                            <span class="help-block">...</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="submit" class="btn btn-dark">
                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                            </form>
                                        </div>

                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body" ng-controller="myCtr">
                        <table id="table_student_1" class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                            <thead>
                            <?php
                            if(isset($_GET['error']) && $_GET['error'] == 1) {
                                echo "<p class = " . "alert-warning" . ">WARNING:  </p>";
                            }
                            ?>
                            <tr>
                                <th> Manage </th>
                                <th> Name </th>
                                <th> Student Code </th>
                                <th> Time interview </th>
                                <th> Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="x in myTable">
                                <td><%1%></td>
                                <td><%x.name%></td>
                                <td><%x.student_id%></td>
                                <td><%x.time_come%></td>
                                <td ng-if="x.is_done == 0" ng-click="changeStatus(x.user_id)"><button>Testing.</button></td>
                                <td ng-if="x.is_done == 1">Tested.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <div class="col-md-4 portlet light" >
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green"></i>
                            <span class="caption-subject font-green bold uppercase">Noti</span>
                            <div class="caption-desc font-grey-cascade"> </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="mt-element-list">
                            <div class="mt-list-head list-simple font-white bg-red">
                            </div>
                            <div id="app2" class="mt-list-container list-simple"  ng-controller="myCtr">
                                <ul>
                                    <li class="mt-list-item" ng-repeat="x in notiList track by $index">
                                        <div class="list-icon-container done">
                                            <i class="icon-check" ng-if="x.time_avai == 'Time out!!' "></i>
                                            <i class="icon-reload" style="color: red" ng-if="x.time_avai != 'Time out!!' "></i>
                                        </div>
                                        <div class="list-datetime" style="width: 90px"> <%x.name%></div>
                                        <div class="list-item-content">
                                            <h3 class="uppercase">
                                                <a href="javascript:;">Time Rest: <%x.time_avai%></a>
                                            </h3>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
@endsection
