@extends('layouts.layout')
@section('content') 

    <div class="page-content">
        <div class="row" >
            <div class="col-md-10 col-md-offset-1 portlet light">
                <!-- END EXAMPLE TABLE PORTLET-->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-dark"></i>
                            <span class="caption-subject font-red sbold uppercase">Table</span>
                        </div>
                        <div class="actions">
                            
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table id="table_student_1" class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                            <thead>
                            <tr>
                                <th> Manage </th>
                                <th> Name </th>
                                <th> Student Code </th>
                                <th> Email </th>
                                <th> Phone number </th>
                                <th> Message </th>
                                <th> Email </th>
                                <th> Rely Message </th>
                                <th> Rely Mail </th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($arr_student as $row)
                                    <tr >
                                        <td>
                                            <button class="btn btn-sm dark" data-toggle="modal" href="#student{{$row->id_student}}">

                                                <i class="fa fa-search"></i>
                                            </button>
                                            <div class="modal fade in" id="student{{$row->id_student}}" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Detail</h4>
                                                        </div>
                                                        <form action="{{ url('notify/'.$row->id)}}" class="form-horizontal" id="form_sample_1" method="post">
                                                        <div class="modal-body">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            
                                                            <div class="form-body">
                                                                <p><strong>Phone:</strong> <span>{{$row->phone_number}}</span></p>
                                                                <p><strong>Email:</strong> <span>{{$row->email}}</span></p>
                                                                <div class="form-group form-md-line-input">
                                                                    <label class="col-md-3 control-label" for="form_control_1">Time interview
                                                                        <span class="required"></span>
                                                                    </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" placeholder="" name="time_interview" value="{{$row->time_interview!=null?$row->time_interview:"" }}">
                                                                        <div class="form-control-focus"> </div>
                                                                        <span class="help-block">time interview</span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group form-md-line-input">
                                                                    <label class="col-md-3 control-label" for="form_control_1">Note CV Online
                                                                        <span class="required">*</span>
                                                                    </label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" class="form-control" placeholder="" name="note_cv_online" value="{{$row->note_cv_online!=null?$row->note_cv_online:"" }}">
                                                                        <div class="form-control-focus"> </div>
                                                                        <span class="help-block">Note Cv online</span>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="md-checkbox-list">
                                                                        <div class="md-checkbox">
                                                                            <div class="col-md-2"></div>
                                                                            <div class="col-md-10">
                                                                                <input type="checkbox" id="message{{$row->id_student}}" name="message" class="md-check" {{(isset($row->is_message)&&$row->is_message==1)?"checked":""}}>
                                                                                <label for="message{{$row->id_student}}">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span> Message </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="md-checkbox">
                                                                            <div class="col-md-2"></div>
                                                                            <div class="col-md-10">
                                                                                <input type="checkbox" id="relymessage{{$row->id_student}}" name="relymessage" class="md-check" {{(isset($row->reply_message)&&$row->reply_message==1)?"checked":""}}>
                                                                                <label for="relymessage{{$row->id_student}}">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span> Rely Message </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="md-checkbox">
                                                                            <div class="col-md-2"></div>
                                                                            <div class="col-md-10">
                                                                                <input type="checkbox" id="email{{$row->id_student}}" name="email" class="md-check" {{(isset($row->is_email)&&$row->is_email==1)?"checked":""}}>
                                                                                <label for="email{{$row->id_student}}">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span> Email </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="md-checkbox">
                                                                            <div class="col-md-2"></div>
                                                                            <div class="col-md-10">
                                                                                <input type="checkbox" id="relyemail{{$row->id_student}}" name="relyemail" class="md-check" {{(isset($row->reply_email)&&$row->reply_email==1)?"checked":""}}>
                                                                                <label for="relyemail{{$row->id_student}}">
                                                                                    <span></span>
                                                                                    <span class="check"></span>
                                                                                    <span class="box"></span> Rely Email </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <div class="row">
                                                                    <div class="col-md-2"></div>
                                                                    <div class="col-md-10" style="margin-top: 20px;">
                                                                        <input type="submit" class="btn btn-dark">
                                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        <td>{{$row->name}}</td>
                                        <td> {{$row->id_student}}</td>
                                        <td> {{$row->email}} </td>
                                        <td> {{$row->phone_number}} </td>
                                        <td> <center>
                                            @if($row->is_message == 1)
                                                <i class='fa fa-check'></i>
                                            @endif
                                        </center> </td>
                                        <td> <center>
                                            @if($row->is_email == 1)
                                                <i class='fa fa-check'></i>
                                            @endif
                                        </center> </td>
                                        <td> <center>
                                            @if($row->reply_message == 1)
                                                <i class='fa fa-check'></i>
                                            @endif
                                        </center> </td>
                                        <td> <center>
                                            @if($row->reply_email == 1)
                                                <i class='fa fa-check'></i>
                                            @endif
                                        </center> </td>
                                        
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>

@endsection