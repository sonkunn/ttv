@extends('layouts.layout')
@section('content')
<div class="page-content">
            <div class="row" >
                <div class="col-md-10 col-md-offset-1 portlet light">
                    <!-- END EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit portlet-datatable bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-dark"></i>
                                <span class="caption-subject font-red sbold uppercase">Table</span>
                            </div>
                            <div class="actions">
                                <button class="btn btn-sm dark" data-toggle="modal" href="#student123">
                                    ADD NEW
                                </button>
                                 <div class="modal fade in" id="student123" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Detail</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{url('/add')}}" class="form-horizontal" id="form_sample_1" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-3 control-label" for="form_control_1">MSV
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="" name="id_student">
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">...</span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-3 control-label" for="form_control_1">CV Offline
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="" name="id_cv_offline">
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">...</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="submit" class="btn btn-dark">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                </form>
                                            </div>

                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table id="table_student_1" class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                                <thead>
                                <tr>
                                    <th> Manage </th>
                                    <th> Name </th>
                                    <th> Student Code </th>
                                    <th style="width: 50px;"> Message </th>
                                    <th style="width: 50px;"> Email </th>
                                    <th style="width: 50px;"> RelyMessage </th>
                                    <th style="width: 50px;"> RelyMail </th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($arr_student as $row)
                                    <tr >
                                        <td>
                                            <button class="btn btn-sm dark" data-toggle="modal" href="#student{{ $row->id_student}}">
                                                <i class='fa fa-pencil'></i>
                                            </button>
                                            <div class="modal fade in" id="student{{ $row->id_student}}" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{url('edit/'.$row->id)}}" class="form-horizontal" id="form_sample_1" method="post">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <div class="form-body">
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">CV OFFLINE
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="text" class="form-control" placeholder="" name="id_cv_offline">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="submit" class="btn btn-dark">
                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                            </form>
                                                        </div>

                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <button class="btn btn-sm dark" data-toggle="modal" href="#student{{ $row->id_student}}">
                                                <i class='fa fa-trash'></i>
                                            </button>
                                            <div class="modal fade in" id="student{{ $row->id_student}}" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <form action="{{url('delete/'.$row->id)}}" class="form-horizontal" id="form_sample_1" method="post">
                                                                <div class="form-body">
                                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-12 control-label" for="form_control_1" style="text-align: center;">
                                                                            ARE YOU SURE?   
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    
                                                                    <div class="col-md-12" style="margin-top: 20px; margin: 0 35%;">
                                                                        <input type="submit" class="btn btn-dark">
                                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        <td> {{$row->name}} </td>
                                        <td> {{$row->id_student}}</td>
                                        <td style="width: 50px;"> <center>
                                            @if($row->is_message==1)
                                            <i class='fa fa-check'></i>
                                            @endif
                                        </center> </td>
                                        <td style="width: 50px;"> <center>
                                            @if($row->is_email==1)
                                            <i class='fa fa-check'></i>
                                            @endif
                                        </center> </td>
                                        <td style="width: 50px;"> <center>
                                            @if($row->reply_message==1)
                                            <i class='fa fa-check'></i>
                                            @endif
                                        </center> </td>
                                        <td style="width: 50px;"> <center>
                                            @if($row->reply_email==1)
                                            <i class='fa fa-check'></i>
                                            @endif
                                        </center> </td>
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>

@endsection