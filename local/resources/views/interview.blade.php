
<head>
    <meta charset="utf-8" />
    <title>Proptit</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('public/assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('public/assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('public/assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{ asset('public/assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{ asset('public/assets/pages/css/login-5.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ asset('public/assets/dist/css/lightbox.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/assets/dist/css/lightbox.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="icon" type="image/x-icon" href="http://127.0.0.1:8000/global/img/Logo.png" />

    <style type="text/css" media="screen">
        ._50 {
            width: 50px;
        }
    </style>

    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>

    <!-- END HEAD -->
</head>
<body class="">
<!-- MENU  -->
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="#">
                </a>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
            <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->
            <div class="hor-menu   hidden-sm hidden-xs">
                <ul class="nav navbar-nav">
                    <!-- DOC: Remove data-hover="megamenu-dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
                    <li class="mega-menu-dropdown">
                        <a href="#"> MENU1
                        </a>
                    </li>
                    <li class="mega-menu-dropdown">
                        <a href="#" class="dropdown-toggle" data-hover="megamenu-dropdown" data-close-others="true"> MENU2
                        </a>
                    </li>
                    <li class="mega-menu-dropdown mega-menu-full" data-hover="megamenu-dropdown" data-close-others="true">
                        <a href="#" class="dropdown-toggle" data-hover="megamenu-dropdown" data-close-others="true"> MENU3
                        </a>
                    </li>
                    <li class="classic-menu-dropdown">
                        <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true">
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END MEGA MENU -->
            <!-- BEGIN HEADER SEARCH BOX -->
            <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
            <form class="search-form" action="#" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search..." name="query">
                    <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                </div>
            </form>
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    
    <!-- MAIN  -->
    <div class="page-container" style="margin-top: 50px">
        <div class="page-content">
            <div class="row" >
                <div class="col-md-12 portlet light">
                    <!-- END EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit portlet-datatable bordered">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-dark"></i>
                                <span class="caption-subject font-red sbold uppercase">Name : MSV </span>
                            </div>
                            <div class="actions">
                                
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div style="margin-bottom: 20px">
                                <button class="btn btn-sm dark" data-toggle="modal" href="#student">
                                    ADD NEW
                                </button>
                                 <div class="modal fade in" id="student" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">Detail</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{url('interview/do_add')}}" class="form-horizontal" id="form_sample_1" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <div class="form-body">
                                                        <div class="form-group form-md-line-input">
                                                            <label class="col-md-2 control-label" for="form_control_1">MSV
                                                                <span class="required">*</span>
                                                            </label>
                                                            <div class="col-md-10">
                                                                <input type="text" class="form-control" placeholder="" name="id_student">
                                                                <div class="form-control-focus"> </div>
                                                                <span class="help-block">...</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="submit" class="btn btn-dark">
                                                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                </form>
                                            </div>

                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            </div>
                            <table id="table_student_1" class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_5">
                                <thead>
                                <tr>
                                    <th>Magager </th>
                                    <th> name </th>
                                    <th> msv </th>
                                    <th> cv </th>
                                    <th> free time  </th>
                                    <th> creative </th>
                                    <th> dynamic </th>
                                    <th> clothes </th>
                                    <th> on time </th>
                                    <th> body language </th>
                                    <th> situation</th>
                                    <th> communication</th>
                                    <th> talent</th>
                                    <th> note</th>
                                    <th> Point</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($arr_interview as $value)
                                    <tr >
                                        <td>
                                            <button class="btn btn-sm dark" data-toggle="modal" href="#edit{{$value->id_student}}">
                                                EDIT
                                            </button>
                                             <div class="modal fade in" id="edit{{$value->id_student}}" tabindex="-1" role="dialog" aria-hidden="true" style="text-align: left;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Detail</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="{{('interview/do_edit/'.$value->id_student)}}" class="form-horizontal" id="form_sample_1" method="post">
                                                                <input type="hidden" name="_token" " value="{{ csrf_token() }}">
                                                                <div class="form-body">
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">CV(10)
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10"class="form-control" placeholder="" name="point_cv" value="{{$value->point_cv}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">free time
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10"  class="form-control" placeholder="" name="point_free_time" value="{{$value->point_free_time}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">creative
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10" class="form-control" placeholder="" name="point_creative" value="{{$value->point_creative}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">dynamic
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10" class="form-control" placeholder="" name="point_dynamic" value="{{$value->point_dynamic}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">clothes
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10" class="form-control" placeholder="" name="point_clothes" value="{{$value->point_clothes}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">on time
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10" class="form-control" placeholder="" name="point_on_time" value="{{$value->point_on_time}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">body language
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10" class="form-control" placeholder="" name="point_body_language" value="{{$value->point_body_language}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">situation
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10" class="form-control" placeholder="" name="point_situation" value="{{$value->point_situation}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">communication
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10" class="form-control" placeholder="" name="point_communication" value="{{$value->point_communication}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">talent
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="number" min="0" max ="10" class="form-control" placeholder="" name="point_talent" value="{{$value->point_talent}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group form-md-line-input">
                                                                        <label class="col-md-3 control-label" for="form_control_1">note
                                                                            <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-9">
                                                                            <input type="text"  class="form-control" placeholder="" name="note" value="{{$value->note}}">
                                                                            <div class="form-control-focus"> </div>
                                                                            <span class="help-block">...</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="submit" class="btn btn-dark">
                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                            </form>
                                                        </div>

                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->msv}}</td>
                                        <td>{{$value->point_cv}}</td>
                                        <td>{{$value->point_free_time}}</td>
                                        <td>{{$value->point_creative}}</td>
                                        <td>{{$value->point_dynamic}}</td>
                                        <td>{{$value->point_clothes}}</td>
                                        <td>{{$value->point_on_time}}</td>
                                        <td>{{$value->point_body_language}}</td>
                                        <td>{{$value->point_situation}}</td>
                                        <td>{{$value->point_communication}}</td>
                                        <td>{{$value->point_talent}}</td>
                                        <td>{{$value->note}}</td>
                                        <td>{{$value->sum_point}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->


<!-- BEGIN CORE PLUGINS -->
<script src="{{ asset('public/assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('public/assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('public/assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('public/assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/pages/scripts/login-5.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/global/plugins/backstretch/jquery.backstretch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/dist/js/lightbox.min.js') }}" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>