var app = angular.module('myApp', []);
app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
app.controller('myCtr', function ($scope, $http, $interval) {
    $scope.reload = function () {
        setTimeout(function () {
            $http({
                method: 'GET',
                url: 'gettable'
            }).then(function (response) {
                $scope.myTable = response.data;
            });
        }, 1000);
        $http({
            method: 'GET',
            url: 'getnoti'
        }).then(function (response) {
            $scope.notiList = response.data;
        });
    };
    $scope.reload();
    $interval($scope.reload, 30000);
    $scope.changeStatus = function ($scope) {
        var r = confirm("OK?");
        if(r) {
            var string = "gettable?action=changeStt&id=" + $scope;
            $http({
                method: 'GET',
                url: string
            }).then(function () {
                window.location.reload();
            });
        };
    };
// $scope.reload1 = function () {
//     $http({
//         method: 'GET',
//         url: 'getNoti.php'
//     }).then(function (response) {
//         $scope.notiList = response.data;
//     });
// };
// $scope.reload1();
});