var app = angular.module('notiApp', []);
app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});
app.controller('notiCtr', function ($scope, $http) {
    $http({
        method: 'GET',
        url: '/getnoti'
    }).then(function (response) {
        $scope.notiList = response.data.records;
    });
});